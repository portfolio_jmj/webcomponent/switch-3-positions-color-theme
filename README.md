# Description
Switch avec trois positions permant de suivre le shémas de couleur de OS ou de le forcer

[démo](https://portfolio_jmj.gitlab.io/webcomponent/switch-3-positions-color-theme)

## Ajout dans une page
1. dans le head de la page ajouter la liasion avec le script rajouter l'attribut **defer** :

```html
<script src="../src/ThemeSwitch.js" defer></script>
```

2. ajout du tag dans le body :

```html 
<theme-switch></theme-switchp>
```

## Evénement
écouter l'événement onChangeTheme et rédupérer la clé theme.
```javascript
el.addEventListener("onChangeTheme", (e)=> {
    switch (e.theme) {
        case "light":

            break;
        case "dark":

            break;            
        default:
            //auto
            break;
    }
})
```

La valeur de theme pourra être : 
- auto  : l'utilisateur souhaite suivre le thème l'os
- light : l'utilisateur souhaite forcer le thème light
- dark  : l'utilisateur souhaite forcer le thème dark
