const Template = document.createElement('template');
Template.innerHTML = `
<style>
input[type=radio]{
    display: none;
}

.container{
    display: flex;
    position: relative;
    width: 144px;
    height: 48px;
    margin: 5px;
    border-radius: 24px;
}

.dark{
    width: 48px;
    height: 48px;
    color: white;
    border-top-left-radius: 24px;
    border-bottom-left-radius: 24px;
}

.auto{
    width: 48px;
    height: 48px;
    color:white;
}

.light{
    width: 48px;
    height: 48px;
    color: black;
    border-top-right-radius: 24px;
    border-bottom-right-radius: 24px;
}

.dark,.auto,.light{
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: transparent;
}

.light span{
    margin-left: 2.5px;
}

.container > label > span{
    width: 27px;
    text-align: center;
}

.position{
    position:absolute;
    height: 40px;
    width: 40px;
    top: 4px;
    box-sizing: border-box;
    border-radius: 50%;
    transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
}

#dark:checked ~ .container .position{
    transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
    left: 4px;
    border: 4px solid white;
}

#dark:checked ~ .container{
    background: black;
}

#dark:checked ~ .container > label{
    color: white;
}

#auto:checked ~ .container .position{
    transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
    left: 52px;
    border: 4px solid rgb(196,196,196);
}

#auto:checked ~ .container{
    background: linear-gradient(90deg, rgba(0,0,0,1) 0%, rgba(255,255,255,1) 100%);
}


#light:checked ~ .container .position{
    transition: all 0.3s cubic-bezier(0.175, 0.885, 0.32, 1.275);
    left: 101px;
    border: 4px solid black;

}

#light:checked ~ .container{
    background: white;
}
#light:checked ~ .container > label{
    color: black;
}

</style>
<input type="radio" name="theme" id="dark"/>
<input type="radio" name="theme" id="auto"/>
<input type="radio" name="theme" id="light"/>
<section class="container">
    <label for="dark" class="dark"><span>☾</span></label>
    <label for="auto" class="auto"><span>auto</span></label>
    <label for="light" class="light"><span>☼</span></label>
    <span class="position"></span>
</section>
`

class ThemeSwitch extends HTMLElement {
    
    dark;
    auto;
    light;
    inputList;
    labelList;
    keyLocalStorage = "themeSwitch";
    
    constructor() {
        super();
        this.attachShadow({ mode: 'open'});
        this.shadowRoot.appendChild(Template.content.cloneNode(true));

        this.dark = this.shadowRoot.getElementById('dark');
        this.auto = this.shadowRoot.getElementById('auto');
        this.light = this.shadowRoot.getElementById('light');
        this.inputList = this.shadowRoot.querySelectorAll("input");
        this.labelList = this.shadowRoot.querySelectorAll('label');
    }
    /**
     * quand le composant est connecté au DOM
     */
    connectedCallback() {
        this.init();
        this.labelList.forEach(element => {
            element.addEventListener('click', () => this.activateMode(element.getAttribute('for')))
        });
    }
    /**
     * Quand le composant sort du DOM
     */
    disconnectedCallback() {
        this.labelList.forEach(element => {
            element.removeEventListener('click', () => this.activateMode(element.getAttribute('for')))
        });
    }

    /**
     * Quand un paramètre change
     * @param {*} name 
     * @param {*} oldVal 
     * @param {*} newVal 
     */
    attributeChangedCallback(name, oldVal, newVal) {
        //implementation
    }
    /**
     * Quand le composant est attribué à un autre je sais pas quoi
     * rien compris pour l'instant
     */
    adoptedCallback() {
    }

    init(){
        const colorSheme = window.localStorage.getItem(this.keyLocalStorage);
        switch (colorSheme) {
            case "dark":
                this.activateRadio("dark");
                break;
            case "light":
                this.activateRadio("light");
                break;
        
            default:
                this.activateRadio("auto");
                break;
        }
    }

    /**
     * Activation du theme de l'affichage du theme
     * @param {string} value 
     */
    activateMode(value){
        this.activateRadio(value);
        window.localStorage.setItem(this.keyLocalStorage,value)
    }

    /**
     * activation du bouton radio
     * @param {string} value 
     */
    activateRadio(value){
        this.inputList.forEach(element => {
            if(element.id == value)
                element.checked = true;
            else
                element.checked = false;
        });
        const event = new Event('onChangeTheme');
        event.theme = value;
        this.dispatchEvent(event);
    }
}

window.customElements.define('theme-switch', ThemeSwitch);